'use strict';

/**
 * @ngdoc overview
 * @name vickApp
 * @description
 * # vickApp
 *
 * Main module of the application.
 */
angular
  .module('vickApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'hljs',
    'ui.bootstrap',
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/xmlviewer', {
        templateUrl: 'views/xmlviewer.html',
        controller: 'XmlViewerCtrl'
      })
      .when('/objviewer', {
        templateUrl: 'views/objviewer.html',
        controller: 'ObjViewerCtrl'
      }).when('/logviewer', {
        templateUrl: 'views/logviewer.html',
        controller: 'LogViewerCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
