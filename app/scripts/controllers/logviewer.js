'use strict';

angular.module('vickApp')
  .controller('LogViewerCtrl', function ($scope) {
  	$scope.loadLog = function(){
  		var rawLines = $scope.rawLog.split(/\r?\n/);
  		$scope.logObjs = [];
  		for(var i in rawLines)
  		{
  			$scope.logObjs[i] = {
  				raw: rawLines[i],
  				format: null,
  				expand: false,
  			};
  		}
  	};
  	$scope.expand = function(index){
  		var logObj = $scope.logObjs[index];
  		if(logObj.format === null){
  			logObj.format = _beautify(logObj.raw);
  		}
  		logObj.expand = true;
  	};
  	$scope.restore = function(index){
  		var logObj = $scope.logObjs[index];
  		logObj.expand = false;
  	};

	$scope.lastHilight = {};
	$scope.hilightColors = ['yellow', 'pink', 'lightgreen', 'skyblue', 'white'];
  	$scope.hilight = function(color){
  		_hilight(color);
  	};

  	$scope.undoHilight = function(){
  		_undoHilight();
  	};

  	function _beautify(input){
    	var output = vkbeautify.xml(input);
    	output=output.split('undefined').join('');
    	return output;
    }

    function _hilight(color){
    	var selection = window.getSelection();
    	if (selection.rangeCount > 0) {
    		var node = selection.getRangeAt(0).startContainer.parentNode;

    		$scope.lastHilight.background = node.style.background;
    		$scope.lastHilight.element = node;

    		node.style.background=color;
    	}
    }

    function _undoHilight(){
    	var elem = $scope.lastHilight.element;
    	elem.style.background = $scope.lastHilight.background;
    }

  });