'use strict';

angular.module('vickApp')
  .controller('XmlViewerCtrl', function ($scope) {
    
	$scope.options = {
		isDecodeHTMLEntity : true,
		isLOMS: false,
	};

    $scope.prettyprint = function(){
    	var input = $scope.rawXml;
    	var output;
    	output = _beautify(input);
    	if($scope.options.isDecodeHTMLEntity){
    		output = _decodeHtmlEntity(output);
    	}
    	if($scope.options.isLOMS){
    		output = _decompressloms(output);
    	}
    	$scope.formattedXml = output;
    };

    $scope.minify = function(){
    	var input = angular.element.find('[source="formattedXml"]');
    	input=input[0].textContent;
    	var output = _minify(input);
    	$scope.minifiedXml = output;
    };

    function _beautify(input){
    	var output = vkbeautify.xml(input);
    	output=output.split('undefined').join('');
    	return output;
    }

    function _minify(input){
    	var output = vkbeautify.xmlmin(input);
    	return output;
    }

    function _decodeHtmlEntity(text){
		var entities = [
	        ['apos', '\''],
	        ['amp', '&'],
	        ['lt', '<'],
	        ['gt', '>'],
			['quot', '"']
	    ];

	    for (var i = 0, max = entities.length; i < max; ++i) {
	        text = text.replace(new RegExp('&'+entities[i][0]+';', 'g'), entities[i][1]);
	    }

	    return text;
	}

	function _decompressloms(compressedxml){
		var map={
			a:	  'data',
			b:    'deal',
			c:    'deal_id',	
			d:    'type',
			e:    'name',
			f:    'instrument',	
			g:    'taker_counterparty',	
			h:    'gid_id',	
			i:    'automonitor',	
			j:    'customer_comments',	
			k:    'customer_comments_line',	
			l:    'customer_ref',
			m:    'customer_ref_line',	
			n:    'dealremarks',	
			o:    'dealremark_line',	
			p:    'entered_by',	
			q:    'entry_date',	
			r:    'execution_style',	
			s:    'fx_cross_deal',	
			t:    'base_amount_dps',	
			u:    'terms_amount_dps',
			v:    'fwd_dps',	
			w:    'side',
			x:    'called_at',	
			y:    'called_by',	
			z:    'fwd_scale_dps',	
			a0:   'unit_quotation',	
			a1:   'ccy',	
			a2:   'ccy1',
			a3:   'ccy2',
			a4:   'ccy_pair',
			a5:   'dealt_ccy',	
			a6:   'dealt_amount',
			a7:   'requirements',
			a8:   'requirement',	
			a9:   'leg',	
			aa:   'cross_component',	
			ab:   'dps',	
			ac:   'indicative_quote',
			ad:   'limitorder_markup',	
			ae:   'settledate',	
			af:   'spot_price',	
			ag:   'spotrate',
			ah:   'all_in',	
			ai:   'quote_breakdown',	
			aj:   'base_spot_markup',
			ak:   'branch_spot_markup',	
			al:   'customer_spot_markup',
			am:   'slippage_spot_markup',
			an:   'volume_spot_markup',	
			ao:   'buy',	
			ap:   'sell',
			aq:   'taker_buys_base',	
			ar:   'value_date',	
			as:   'expiry_date',	
			at:   'good_from',	
			au:   'good_till',	
			av:   'gft_location',
			aw:   'maker_name',	
			ax:   'maker_group_name',
			ay:   'non_negotiated',	
			az:   'oco_partner',	
			b0:   'on_filled',	
			b1:   'order_observers',	
			b2:   'parent',	
			b3:   'price_reached',	
			b4:   'proxy_full_name',	
			b5:   'proxy_institution',	
			b6:   'proxy_name',	
			b7:   'sub_type',
			b8:   'maker_account',	
			b9:   'taker_account',	
			ba:   'taker_full_name',	
			bb:   'taker_group_name',
			bc:   'taker_institution',	
			bd:   'taker_name',	
			be:   'taker_version',	
			bf:   'timed_out',	
			bg:   'unregistered_taker_id',	
			bh:   'unregistered_taker_name',	
			bi:   'version',	
			bj:   'fill_data',	
			bk:   'lock_owner',	
			bl:   'owner_data',	
			bm:   'accepted',
			bn:   'default_sales_group',	
			bo:   'last_action_by',	
			bp:   'last_action_time',
			bq:   'last_passer',	
			br:   'last_userset',
			bs:   'origin',	
			bt:   'passer',	
			bu:   'stolen',	
			bv:   'style',	
			bw:   'to_userset',	
			bx:   'slippage',
			by:   'state',	
			bz:   'markup_breakdown_core',	
			c0:   'deal_reference',	
			c1:   'amount',	
			c2:   'amount_done',	
			c3:   'rate',
			c4:   'rates',	
			c5:   'fill_deal_remark',
			c6:   'order_create',
			c7:   'taker_order_reference',	
			c8:   'standard',
			c9:   'settlement_details',	
			ca:   'line',
			cb:   'in_progress',	
			cc:   'notification',
			cd:   'internal',
			ce:   'taker_memo',	
			cf:   'fill',
			cg:   'requires_attention',	
			ch:   'error_code',	
			ci:   'reason',	
			cj:   'private_data',
			ck:   'live_status',	
			cl:   'fwd_pts',	
			cm:   'filled_by',	
			cn:   'fill_date',	
			co:   'fill_time',	
			cp:   'spot_markup',	
			cq:   'fwd_markup',	
			cr:   'markup_breakdown',
			cs:   'base_fwd_markup',	
			ct:   'volume_fwd_markup',	
			cu:   'branch_fwd_markup',	
			cv:   'customer_fwd_markup',	
			cw:   'spot_date',	
			cx:   'strategy_id',	
			cy:   'username',
			cz:   'user_type',	
			d0:   'notification_list',	
			d1:   'group_structure',	
			d2:   'fullname',
			d3:   'location',
			d4:   'fixing_code',	
			d5:   'pms_id',	
			d6:   'quantity_outstanding',
			d7:   'quantity_filled',	
			d8:   'temperature',	
			d9:   'quote',	
			da:   'amount_outstanding',	
			db:   'last_maker_name',	
			dc:   'last_owned_by',	
			dd:   'rounddown_ccys',	
			de:   'routing',	
			df:   'entry_time',	
			dg:   'event_code',	
			dh:   'report_to',	
			di:   'upload_flags',
			dj:   'order_type',	
			dk:   'deal_type',	
			dl:   'taker_email',	
			dm:   'blockticket_id',	
			dn:   'related_swap_tr',	
			do:   'contra_amount',	
			dp:   'account_long_name',	
			dq:   'account_group_name',	
			dr:   'taker_custom',
			ds:   'internal_name',	
			dt:   'equiv',	
			du:   'last_prime_by',	
			dv:   'last_prime_time',	
			dw:   'post_trade_state',
			dx:   'proxy_own_filter',
			dy:   'payment_method',	
			dz:   'failure_reason',	
			e0:   'error_text',	
			e1:   'trade_requirements',	
			e2:   'remarks',	
			e3:   'captured_by',	
			e4:   'last_submit_by',	
			e5:   'last_submit_time',
			e6:   'profit_amount',	
			e7:   'equiv_amount',
			e8:   'rate1',	
			e9:   'rate2',	
			ea:   'trade_requirement',	
			eb:   'remark_line',	
			ec:   'pair',
			ed:   'bid',	
			ee:   'ask',	
			ef:   'useDPS',	
			eg:   'bignum_offset',	
			eh:   'bignums',	
			ei:   'spot_markup_dps',	
			ej:   'underlying_side',	
			ek:   'quote_expiry',
			el:   'spot',
			em:    'fwds'
		};
		var newxml=compressedxml;
		for(var key in map){
			newxml=newxml.replace(new RegExp('<'+key+'>'  , 'g'),'<'+map[key]+'>');
			newxml=newxml.replace(new RegExp('<'+key+' '  , 'g'),'<'+map[key]+' ');
			newxml=newxml.replace(new RegExp(' '+key+'='  , 'g'),' '+map[key]+'=');
			newxml=newxml.replace(new RegExp('</'+key+'>' , 'g'),'</'+map[key]+'>');
			newxml=newxml.replace(new RegExp('<'+key+'/>' , 'g'),'<'+map[key]+'/>');
			newxml=newxml.replace(new RegExp('<'+key+' />', 'g'),'<'+map[key]+' />');
		}
		return newxml;
	}


  });