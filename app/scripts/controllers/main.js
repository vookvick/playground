'use strict';

/**
 * @ngdoc function
 * @name vickApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the vickApp
 */
angular.module('vickApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
