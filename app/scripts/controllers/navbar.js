'use strict';

angular.module('vickApp')
  .controller('NavBarCtrl', function ($scope, $window) {

  	$scope.changeTitle = function(title) {
  		$window.document.title = title;
  	};

  });